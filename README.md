# Tierwave

a tier list app

## Contribution requirement:
if you, for any reason, want to contribute to this project, that's fine! just now that I somewhat rigorously follow the roadmap, with '6' features per release, and I won't publish a new version until those 6 features are done, if they are possible

## ROADMAP for 0.3:
-[ ] import & export .yaml files\
-[ ] import list as json\
-[ ] select list items\
-[ ] custom tiers\
-[ ] grid layout\
-[ ] save lists in app storage\
