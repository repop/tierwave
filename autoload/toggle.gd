extends Node

func view(node):
	if node.is_visible() == true:
		node.hide()
	else:
		node.show()
		
func editable(node):
	if node.is_editable() == true:
		node.editable = false
	else:
		node.editable = true

