extends Control

var goback
var onPressed = [_godot, _ubuntu, _wave]
# Called when the node enters the scene tree for the first time.
func _ready():
	goback = $Field/Title/goback
	for n in len($FlowContainer.get_children()):
		$FlowContainer.get_child(n).pressed.connect(onPressed[n])
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_goback_pressed():
	queue_free()
	pass # Replace with function body.

func _godot():
	OS.shell_open('https://godotengine.org/license/')
	pass

func _ubuntu():
	OS.shell_open('https://ubuntu.com/legal/font-licence')
	pass
	
func _wave():
	OS.shell_open('https://www.fontspace.com/wave-font-f5912')
