extends Sprite2D

var counter := 0.0
var minBright := 0.5
@export var speed := Vector2i(0, 0)
# Called when the node enters the scene tree for the first time.
func _ready():
	var bigger = max(float(get_viewport().size.x) / 720, float(get_viewport().size.y) / 720) *3
	scale = Vector2(bigger, bigger)
	if get_parent().get_node('l3') == self:
		modulate = Color(randf() /2 + minBright, randf() /2 + minBright, randf() /2 + minBright)
	get_viewport().size_changed.connect(_resize)
	pass # Replace with function body.
	
func _resize():
	var bigger = max(float(get_viewport().size.x) / 720, float(get_viewport().size.y) / 720) *3
	#print(bigger)
	scale = Vector2(bigger, bigger)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	counter += 1 * delta
	counter = min(counter, 3)
	if counter == 3 and get_parent().get_node('l3') == self:
		var tween = create_tween()
		tween.tween_property(self, 'modulate', Color(randf() /2 + minBright, randf() /2 + minBright, randf() /2 + minBright), 0.8)
		counter = 0
	texture.region.position += delta * speed
	pass
