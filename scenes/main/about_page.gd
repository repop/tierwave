extends Button

var mainNode
var aboutInstance
var aboutPage = preload('res://scenes/about/about_page.tscn')
# Called when the node enters the scene tree for the first time.
func _ready():
	pressed.connect(_on_pressed)
	mainNode = get_tree().root.get_node('Main')
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_pressed():
	aboutInstance = aboutPage.instantiate()
	mainNode.add_child(aboutInstance)
	pass # Replace with function body.

	pass # Replace with function body.
