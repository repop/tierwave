extends Button

var list
var item = preload('res://scenes/item/item.tscn')
# Called when the node enters the scene tree for the first time.
func _ready():
	list = get_parent().get_parent().get_node('ScrollContainer/Sepman/List')
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_pressed():
	var itemInstance = item.instantiate()
	list.add_child(itemInstance)
	pass # Replace with function body.
