extends MenuButton

var funcs = [exportJSON,
exportMark]
var image
# Called when the node enters the scene tree for the first time.
func _ready():
	get_popup().transparent = true
	get_popup().transparent_bg = true
	get_popup().id_pressed.connect(_on_item_selected)
	get_popup().set_item_text(1, tr('MARKDOWN'))
	get_popup().set_item_text(0, tr('JSON'))
	pass # Replace with function body.

func _on_item_selected(id):
	if OS.get_name() != 'Android':
		for n in 2:
			if id == n:
				funcs[n].call()
	pass

func exporter():
	var export := {'S': {}, 'A': {}, 'B': {}, 'C': {}, 'D': {}, 'noTier': {}}
	var tiers := [' S', ' A', ' B', ' C', ' D']
	for n in get_tree().get_nodes_in_group('list'):
		for item in n.get_children():
			var itemName = item.get_node('Info/MainText/Name').text
			var itemDesc = item.get_node('bout/Desc').text
			var tierNode = item.get_node('Info/Tiers')
			var itemImg = item.get_node('bout/Pic').texture
			for tier in len(tiers):
				if tierNode.get_item_text(tierNode.get_selected_id()) == tiers[tier]:
					export[tiers[tier].replace(' ', '')][itemName] = {'desc': itemDesc}
				elif tierNode.get_selected_id() == -1:
					export['noTier'][itemName] = {'desc': itemDesc}
	return export

func exportJSON():
	var export = exporter()
	pass
	var count = 0
	while FileAccess.open('export_' + get_parent().get_node('appName').text + str(count)\
	 + '.json', FileAccess.READ):
		count += 1
	var file = FileAccess.open('export_' + str(count) + '.json', FileAccess.WRITE)
	file.store_string(JSON.stringify(export).replace(',', ',\n').replace('}}', '}\n}'))
	#print(export)
	pass

func exportMark():
	var export = exporter()
	var count := 0
	var exportString := ''
	while FileAccess.open('export_' + str(count) + '.md', FileAccess.READ):
		count += 1
	var file = FileAccess.open('export_' + str(count) + '.md', FileAccess.WRITE)
	var title = '# ' + get_parent().get_node('appName').text + '\n'
	exportString = title
	for n in export:
		exportString += '## Tier ' + n + '\n'
		for item in export[n]:
			exportString += '### ' + item + '\n'
			for thing in export[n][item]:
				exportString += '#### ' + thing + '\n'
				exportString += export[n][item][thing] + '\n'
				pass
		pass
	file.store_string(exportString)
	pass
	
#func exportPNG():
	#var x = get_viewport_rect().position.x
	#var y = get_viewport_rect().position.y +60
	#var h = get_viewport_rect().size.y +1000
	#var w = get_viewport_rect().size.x
	#var region = Rect2(x, y, w, h) # change the values around to suit your actual scene
	#print(get_viewport().get_texture().get_image())
	#image = get_viewport().get_texture().get_image().get_region(region)
	##image.flip_y()
	#var timer = get_tree().create_timer(7)
	#timer.timeout.connect(_shot_me)
	#pass

#func _shot_me():
#	image.save_png("screenshot.png")
#	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_pressed():
	get_popup().get_window().position -= Vector2i(43, -10)
	pass # Replace with function body.

