extends BoxContainer

var child_count := 0
var ordering := []
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if get_child_count() != child_count:
		for n in get_children():
			if not n.get_node('Info/Tiers').is_connected('item_selected', _on_item_selected):
				n.get_node('Info/Tiers').item_selected.connect(_on_item_selected)
		child_count = get_child_count()
	pass
	
func _on_item_selected(_index):
	ordering = []
	for num in 5:
		for n in get_children():
			if n.get_node('Info/Tiers').value == num:
				ordering += [n]
	for n in len(ordering):
		move_child(ordering[n], n)
