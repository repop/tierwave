extends MenuButton

signal check_bout
signal switch_pic_text
signal switch_desc_text
var bout
var funcs := [func(): DisplayServer.clipboard_set(get_parent().get_node('MainText/Name').text),
func(): get_parent().get_node('MainText/Name').text = DisplayServer.clipboard_get(),
func(): toggle.view(bout.get_node('Pic')); check_bout.emit(); switch_pic_text.emit(),
func(): toggle.view(bout.get_node('Desc')); check_bout.emit(); switch_desc_text.emit(),
func(): get_parent().get_parent().queue_free()]
# Called when the node enters the scene tree for the first time.
func _ready():
	if OS.get_name() == 'Android':
		funcs.remove_at(2)
		get_popup().remove_item(2)
	get_popup().index_pressed.connect(_on_item_selected)
	bout = get_parent().get_parent().get_node('bout')
	check_bout.connect(_check_bout)
	switch_pic_text.connect(_switch_pic_text)
	switch_desc_text.connect(_switch_desc_text)
	get_popup().transparent = true
	get_popup().transparent_bg = true
	pass # Replace with function body.

func _check_bout():
	if not bout.get_node('Pic').is_visible()\
and not bout.get_node('Desc').is_visible():
		bout.hide()
	else:
		bout.show()
		pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_item_selected(index):
	for n in len(funcs):
		if index == n:
			funcs[n].call()
	pass # Replace with function body.
	
func _switch_pic_text():
	print(get_popup().get_item_text(2))
	if get_popup().get_item_text(2) == tr('SHOW_PIC')\
or get_popup().get_item_text(2) == 'SHOW_PIC':
		print('d')
		get_popup().set_item_text(2, tr('HIDE_PIC'))
	else:
		get_popup().set_item_text(2, tr('SHOW_PIC'))

func _switch_desc_text():
	var index = 3
	if OS.get_name() == 'Android':
		index -= 1
	if get_popup().get_item_text(index) == tr('SHOW_DESC')\
or get_popup().get_item_text(index) == 'SHOW_DESC':
		get_popup().set_item_text(index, tr('HIDE_DESC'))
	else:
		get_popup().set_item_text(index, tr('SHOW_DESC'))

func _on_pressed():
	get_popup().get_window().position += Vector2i(0, 10)
	pass # Replace with function body.
