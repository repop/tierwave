extends OptionButton

var value := 999
var lastValue := value
var tiers := [' S', ' A', ' B', ' C', ' D']
@export var moduls = [Color(1.6, 1.2, 0.3, 1), Color(1.0, 1.0, 1.15, 1), 
Color(1.2, 0.7, 0.6, 1), Color(1.2, 1, 0.5, 1), Color(0.9, 0.9, 0.9, 1)]
# Called when the node enters the scene tree for the first time.
func _ready():
	get_popup().transparent = true
	get_popup().transparent_bg = true
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func _on_item_selected(index):
	for n in len(tiers):
		if get_item_text(index) == tiers[n]:
			value = n
			get_parent().modulate = moduls[n]
	pass # Replace with function body.

func _on_pressed():
	get_popup().get_window().position -= Vector2i(43, -10)
	pass # Replace with function body.
