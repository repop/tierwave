extends TextureRect

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_gui_input(event):
	var main = get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().get_parent()
	if event is InputEventMouseButton and event.is_pressed() == false:
		if OS.get_name() != 'Android':
			var picker = FileDialog.new()
			picker.position = Vector2i(-60, 0)
			picker.size = get_viewport().size
			picker.file_mode = FileDialog.FILE_MODE_OPEN_FILE
			picker.access = 2
			picker.use_native_dialog = true
			picker.file_selected.connect(_new_image)
			picker.initial_position = 2
			picker.file_mode = 0
			picker.filters = ['*.png, *.jpg; Images']
			picker.show()
			main.add_child(picker)
		else:
			get_parent().get_parent().get_node('Info/MainText/Name').text = tr('NO_FEATURE')
	pass # Replace with function body.

func _new_image(path):
	var img = Image.new()
	img.load(path)
	var tex = ImageTexture.new()
	tex.set_image(img)
	texture = tex
	pass
